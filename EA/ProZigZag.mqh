//+------------------------------------------------------------------+
//|                                                   EasyZigZag.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property strict
#include <Indicators\Custom.mqh>
#include <Arrays\ArrayString.mqh>
#include <ChartObjects\ChartObjectsLines.mqh>
#include <stdlib.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum SWING_TYPE
{
   SWING_HIGH,
   SWING_LOW,
   SWING_ERROR
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
#include <Arrays\ArrayObj.mqh>
template<typename T>
class objvector : public CArrayObj
{
public:
   T operator[](const int index) const { return At(index); }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class DrawList : public objvector<CChartObject*>
{
public:
   CChartObjectTrend* Search(const string name)
   {
      for(int i=0;i<Total();i++)
         if(this[i].Name() == name)
            return this[i];
      return NULL;
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CZigData : public CObject
{
public:
                     CZigData(){}
                     CZigData(SWING_TYPE        swing,
                              double            price,
                              datetime          time,
                              ENUM_TIMEFRAMES   periodRes,
                              int               zigzagDepth
                           ):
                              swingType         (swing),
                              swingPrice        (price),
                              swingTime         (time),
                              swingResolution   (periodRes),
                              depth             (zigzagDepth)
                           {}
   SWING_TYPE        swingType; // -1 swinglow +1 swinghigh
   double            swingPrice;
   datetime          swingTime;
   ENUM_TIMEFRAMES   swingResolution;
   int               depth;
   int  Compare(const CObject *node,const int mode=0)const override
   {
      CZigData *other = (CZigData*)node;
      if(this.swingTime > other.swingTime) return -1; //sort greatest to least; most current first
      if(this.swingTime < other.swingTime) return 1;
      return 0;
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CZigDataArray : public CArrayObj
{
public:
   CZigData*         operator[](const int i) const { return (CZigData*)this.At(i); }
   bool              Add(  SWING_TYPE        swing,
                           double            price,
                           datetime          time,
                           ENUM_TIMEFRAMES   periodRes,
                           int               zigzagDepth
                        );
};
//+------------------------------------------------------------------+
bool CZigDataArray::Add(SWING_TYPE swing,double price,datetime time,ENUM_TIMEFRAMES periodRes,int zigzagDepth)
{
   return Add(new CZigData(swing,price,time,periodRes,zigzagDepth));
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CSwingSet : public CObject
{
protected:
   SWING_TYPE        m_swingType; // -1 swinglow +1 swinghigh
   double            m_priceBegin;
   double            m_priceEnd;
   datetime          m_timeBegin;
   datetime          m_timeEnd;
   ENUM_TIMEFRAMES   m_swingResolution;
   int               m_depth;
public:
                     CSwingSet(){}
                     CSwingSet(SWING_TYPE       swing,
                              datetime          sTime,
                              double            sPrice,
                              datetime          eTime,
                              double            ePrice,
                              ENUM_TIMEFRAMES   periodRes,
                              int               zigzagDepth
                           ):
                              m_swingType         (swing),
                              m_timeBegin         (sTime),
                              m_priceBegin        (sPrice),
                              m_timeEnd           (eTime),
                              m_priceEnd          (ePrice),
                              m_swingResolution   (periodRes),
                              m_depth             (zigzagDepth)
                           {}
   CSwingSet*        SwingType(const SWING_TYPE type)             { m_swingType        = type;   return &this;}
   CSwingSet*        PriceBegin(const double price)               { m_priceBegin       = price; return &this;   }
   CSwingSet*        PriceEnd(const double price)                 { m_priceEnd         = price; return &this;   }
   CSwingSet*        TimeBegin(const datetime time)               { m_timeBegin        = time;  return &this;   }
   CSwingSet*        TimeEnd(const datetime time)                 { m_timeEnd          = time;  return &this;   }
   CSwingSet*        SwingResolution(const ENUM_TIMEFRAMES period){ m_swingResolution  = period; return &this;  }
   CSwingSet*        Depth(const int depth)                       { m_depth            = depth;  return &this;  }
   SWING_TYPE        SwingType()                            const { return m_swingType;            }
   double            PriceBegin()                           const { return m_priceBegin;           }
   double            PriceEnd()                             const { return m_priceEnd;             }
   datetime          TimeBegin()                            const { return m_timeBegin;            }
   datetime          TimeEnd()                              const { return m_timeEnd;              }
   ENUM_TIMEFRAMES   SwingResolution()                      const { return m_swingResolution;      }
   int               Depth()                                const { return m_depth;                }
   
   string            ToString();
   
};
//+------------------------------------------------------------------+
string CSwingSet::ToString(void)
{
   return ( SwingType() == SWING_HIGH ? 
            "Swing Low ("+DoubleToString(PriceBegin())+") -> Swing High ("+DoubleToString(PriceEnd())+")" :
            "Swing High ("+DoubleToString(PriceBegin())+") -> Swing Low ("+DoubleToString(PriceEnd())+")"
            );
}
//+------------------------------------------------------------------+
class CSwingSetList : public objvector<CSwingSet*>
{
public:
   bool              Add(  SWING_TYPE        swing,
                           datetime          sTime,
                           double            sPrice,
                           datetime          eTime,
                           double            ePrice,
                           ENUM_TIMEFRAMES   periodRes,
                           int               zigzagDepth
                        );
};
//+------------------------------------------------------------------+
bool CSwingSetList::Add(SWING_TYPE swing,datetime sTime,double sPrice,datetime eTime,double ePrice,ENUM_TIMEFRAMES periodRes, int zigzagDepth)
{
   return Add(new CSwingSet(swing,sTime,sPrice,eTime,ePrice,periodRes,zigzagDepth));
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//#resource "\\Indicators\\ProZigZag.ex4"

class CProZigZag : public CIndicator
{
protected:
   
   int               m_custom_buffer;
   int               m_depth;
   int               m_digits;
   double            m_point;
   int               m_deviation;
   //int               m_backstep;
   
   CSwingSetList     m_SwingSetList;
  
   DrawList          m_drawList;
   bool              m_draw_on_chart;
   int               m_lastNbars;
   int               m_maxWorkingBuffer;
   ENUM_TIMEFRAMES   m_tfList[];
   ENUM_TIMEFRAMES   m_lowTFResoltion;
   color             m_color;
   bool              m_resolveToLowest;
   string            m_instance;
   static int        m_num_instance;

public:  
                     CProZigZag(void);
                     CProZigZag(const string symbol,const ENUM_TIMEFRAMES timeframe,const int depth);
   //--- get methods    
   CZigDataArray     ZigZagList;           
   CSwingSet*        operator[](const int i)       const { return (CSwingSet*)m_SwingSetList[i];    }
   int               BufferSize()                  const { return m_maxWorkingBuffer ;       }
   virtual double    GetData(const int buffer_num,const int index) const override;
   int               Total()                       const { return m_SwingSetList.Total();          }     
   virtual int       Type(void)                    const { return IND_CUSTOM;                } 
   ENUM_TIMEFRAMES   Period()                      const { return (ENUM_TIMEFRAMES)m_period;                  }
   int               Depth()                       const { return m_depth;                   }
   //--- set methods
   bool              Create(const string symbol,const ENUM_TIMEFRAMES period,const int depth);
   virtual bool      BufferResize(const int size)              override;
   virtual void      Refresh(const int flags=OBJ_ALL_PERIODS)  override;  
   virtual void      DrawOnChart(const bool draw_on_chart);
   //virtual void      RemoveFromChart();
   void              ResolveToLowestTimeFrame(const bool resolve) { m_resolveToLowest = resolve;}
   void              SetColor(const color col)                 { m_color = col; }
   void              SetCustomIndName(const string name)       { m_name = name;}
   void              SetCustomIndBufferGetIndex(const int i)   { m_custom_buffer = i;}

protected:
   virtual bool      Initialize(const string symbol,const ENUM_TIMEFRAMES period,const int num_params,const MqlParam &params[]);
   bool              Initialize(const string symbol,const ENUM_TIMEFRAMES period,const int ma_period);
   void              InitTFlist();
   void              Draw();
   bool              SetEasyZigZagBuffer(bool force_refresh = false);
   bool              FixResolution();
   int               BarCount(const ENUM_TIMEFRAMES tf);
   bool              RefreshZigData(bool force_refresh = false);
   void              SetSwingSets();
   int               SetTFArray(ENUM_TIMEFRAMES &tf[],datetime swingTime);   
};
int CProZigZag::m_num_instance=0;
//+------------------------------------------------------------------+
CProZigZag::CProZigZag(void):                   
   m_custom_buffer   (0),
   m_deviation       (5),
   //m_backstep        (3),
   m_maxWorkingBuffer(10),
   m_lowTFResoltion  (PERIOD_M1),
   m_color           (clrRed),
   m_resolveToLowest (true)
   //m_instance        ((string)GetMicrosecondCount())
{
    m_name = "ProZigZag";
    m_num_instance++;
    m_draw_on_chart = false;
    m_instance = (string)m_num_instance;
}
//+------------------------------------------------------------------+
CProZigZag::CProZigZag(const string            symbol,
                           const ENUM_TIMEFRAMES   timeframe,
                           const int               depth ):
  
   m_custom_buffer   (0),
   m_deviation       (5),   
   //m_backstep        (3),
   m_maxWorkingBuffer(10),
   m_lowTFResoltion  (PERIOD_M1),
   m_color           (clrRed),
   m_resolveToLowest (true)
   //m_instance        (IntegerToString((int)GetTickCount()))
{
   m_name = "ProZigZag";
   m_num_instance++;
   m_draw_on_chart = false;
   m_instance = (string)m_num_instance;
   if(!Create(symbol,timeframe,depth))
   {
      ::Print(__FUNCTION__,ErrorDescription(::GetLastError()));
   }
}
//+------------------------------------------------------------------+
bool CProZigZag::Create(const string symbol,const ENUM_TIMEFRAMES period,const int depth)
{
   SetSymbolPeriod(symbol,period);
   return(Initialize(symbol,period,depth));
}

//+------------------------------------------------------------------+
bool CProZigZag::Initialize(const string symbol,const ENUM_TIMEFRAMES period,const int num_params,const MqlParam &params[])
{
   return(Initialize(symbol,period,(int)params[0].integer_value));
}
//+------------------------------------------------------------------+
bool CProZigZag::Initialize(const string symbol,const ENUM_TIMEFRAMES period,const int depth)
{
   InitTFlist();
   
   m_point  = ::SymbolInfoDouble(m_symbol,SYMBOL_POINT);
   m_digits = (int)::SymbolInfoInteger(m_symbol,SYMBOL_DIGITS);
//--- save settings
   m_depth=depth;
//--- ok
   RefreshZigData();
   return(true);
}
//+------------------------------------------------------------------+
void CProZigZag::InitTFlist(void)
{
   const ENUM_TIMEFRAMES tf[9] = { PERIOD_M1,PERIOD_M5,PERIOD_M15,PERIOD_M30,PERIOD_H1,PERIOD_H4,PERIOD_D1,PERIOD_W1,PERIOD_MN1 };   
   ::ArrayCopy(m_tfList,tf);
}
//+------------------------------------------------------------------+
double CProZigZag::GetData(const int buffer_num,const int index) const
{
   return ::iCustom(m_symbol,m_period,m_name,m_depth,m_deviation,buffer_num,index);
}

//+------------------------------------------------------------------+
bool CProZigZag::BufferResize(const int size) override
{
   m_maxWorkingBuffer = size;
   RefreshZigData(true);
   return true;
}
//+------------------------------------------------------------------+
void CProZigZag::Refresh(const int flags=OBJ_ALL_PERIODS) override
{
   RefreshZigData();
}
//+------------------------------------------------------------------+
bool CProZigZag::RefreshZigData(bool force_refresh = false)
{
   m_lastNbars = ::iBars(m_symbol,m_period);
   //SetEasyZigZagBuffer();
   if(SetEasyZigZagBuffer(force_refresh))
   {
      if(m_resolveToLowest)
         FixResolution(); 
      ZigZagList.Sort();
      SetSwingSets();
      if(m_draw_on_chart)
         Draw();
      return true; // new data! // bool check not currently implemented
   } 
   return false; // no new data
}
//+------------------------------------------------------------------+
bool  CProZigZag::SetEasyZigZagBuffer(bool force_refresh = false)
{
   //ZigZagList.Clear();
   int cnt = 0;
   for(int i=0;i<m_lastNbars;i++)
   {
      if(ZigZagList.Total() >= m_maxWorkingBuffer && cnt > 0)
         break;
      double temp_up = GetData(0,i);
      double temp_dn = GetData(1,i);
      if(temp_up > 0.0 && temp_up != EMPTY_VALUE && temp_up < 100000.0)
      {
         SWING_TYPE type = SWING_HIGH;
         //double close = ::iClose(m_symbol,m_period,i);
         //double open  = ::iOpen(m_symbol,m_period,i);
         //double low   = ::iLow(m_symbol,m_period,i);
         //if( close >= temp || (close == temp && close < open) || (close == temp && open == temp && close == low) )
         //   type = SWING_LOW;     
         //else 
         //   type = SWING_HIGH;
            
         datetime        time  = ::iTime(m_symbol,m_period,i);
         double          price = ::NormalizeDouble(temp_up,m_digits);
         ENUM_TIMEFRAMES res   = PERIOD_CURRENT;
         if(cnt == 0 && ZigZagList.Total() > 0)
         {
            if(!force_refresh && ZigZagList[0].swingPrice == price && ZigZagList[0].swingType == type)
               return false;  
            ZigZagList.Clear();
         }
         cnt++;
         ZigZagList.Add(type,price,time,res,m_depth);
      }
      if(temp_dn > 0.0 && temp_dn != EMPTY_VALUE && temp_dn < 100000.0)
      {
         SWING_TYPE type = SWING_LOW;
//         double close = ::iClose(m_symbol,m_period,i);
//         double open  = ::iOpen(m_symbol,m_period,i);
//         double low   = ::iLow(m_symbol,m_period,i);
//         if( close >= temp || (close == temp && close < open) || (close == temp && open == temp && close == low) )
//            type = SWING_LOW;     
//         else 
//            type = SWING_HIGH;
//            
         datetime        time  = ::iTime(m_symbol,m_period,i);
         double          price = ::NormalizeDouble(temp_dn,m_digits);
         ENUM_TIMEFRAMES res   = PERIOD_CURRENT;
         if(cnt == 0 && ZigZagList.Total() > 0)
         {
            if(!force_refresh && ZigZagList[0].swingPrice == price && ZigZagList[0].swingType == type)
               return false;  
            ZigZagList.Clear();
         }
         cnt++;
         ZigZagList.Add(type,price,time,res,m_depth);
      }
   }
   return true;
}
//+------------------------------------------------------------------+
void CProZigZag::SetSwingSets(void)
{
   m_SwingSetList.Clear();
   for(int i=0;i<ZigZagList.Total() -1;i++)
   {
      m_SwingSetList.Add(  ZigZagList[i]   .swingType,
                           ZigZagList[i+1] .swingTime,
                           ZigZagList[i+1] .swingPrice,
                           ZigZagList[i]   .swingTime,
                           ZigZagList[i]   .swingPrice,
                           ZigZagList[i+1] .swingResolution,
                           ZigZagList[i]   .depth
                        );
   }
}
//+------------------------------------------------------------------+
bool CProZigZag::FixResolution(void)
{
   int arrSize = ZigZagList.Total();
   ENUM_TIMEFRAMES tfs[];
   //--- for each zigzag
   for(int i=0;i<arrSize;i++) 
   {
      int nTimeFrames = SetTFArray(tfs,ZigZagList[i].swingTime);    // sets the tfs array to the min thru max timeframe for resolution steps
      for(int tf = nTimeFrames-1; tf>=0;tf--)                    // for each timeframe in array from lowest to highest resolution
      {
         int startBar     = 0;
         int barIteration = 0;
         ENUM_TIMEFRAMES timeFrameDebug = tfs[tf];
         datetime        swingTimeDebug = ZigZagList[i].swingTime;
         startBar = ::iBarShift(m_symbol,tfs[tf],ZigZagList[i].swingTime);
         //--- 
         if(startBar > -1)
         {
            int bCnt = BarCount(tfs[tf]);
            if(ZigZagList[i].swingType == SWING_HIGH)
            { // is swing high
               barIteration=0;
               
               for(int j=startBar; j >= startBar-bCnt; j--)
               {
                  double ihigh = ::NormalizeDouble(iHigh(m_symbol,tfs[tf],j),m_digits);
                  double swing = ZigZagList[i].swingPrice;
                  if( ihigh == swing )
                  {
                     startBar = startBar - barIteration;
                     datetime newSwingTime = ::iTime(m_symbol,tfs[tf],j);
                     ZigZagList[i].swingTime = newSwingTime;
                     timeFrameDebug = tfs[tf];
                     ZigZagList[i].swingResolution = tfs[tf];
                     break;
                  }
                  barIteration++;
               }
               //break; //testing break in case of malfunction
            }
            else if(ZigZagList[i].swingType == SWING_LOW)
            { // is swing low
               barIteration = 0;
               for(int j=startBar; j >= startBar-bCnt; j--)
               { 
                  double ilow = ::NormalizeDouble(iLow(m_symbol,tfs[tf],j),m_digits);
                  double swing = ZigZagList[i].swingPrice;
                  if(ilow == swing )
                  {
                     startBar = startBar - barIteration;
                     datetime newSwingTime = ::iTime(m_symbol,tfs[tf],j);
                     ZigZagList[i].swingTime = newSwingTime;
                     timeFrameDebug = tfs[tf];
                     ZigZagList[i].swingResolution = tfs[tf];
                     break;
                  }
                  barIteration++;
               }
               //break; // testing
            }
         }
      }
   }
   return(true);
}

//+------------------------------------------------------------------+
int CProZigZag::BarCount(const ENUM_TIMEFRAMES tf)
{
   switch(tf)
   {
      case PERIOD_M1:
         return 5;
      case PERIOD_M5:
         return 3;
      case PERIOD_M15:
         return 2;
      case PERIOD_M30:
         return 2;
      case PERIOD_H1:
         return 4;
      case PERIOD_H4:
         return 6;
      case PERIOD_D1:
         return 6;
      case PERIOD_W1:
         return 4;
      default:
         return 1;
   }
   return -1;
}

//+------------------------------------------------------------------+
int CProZigZag::SetTFArray(ENUM_TIMEFRAMES &tf[],datetime swingTime)
{
   static datetime maxTime[9]={0};
   for(int i=0;i<ArraySize(maxTime);i++)
   {
      maxTime[i] = (datetime)SeriesInfoInteger(m_symbol,m_tfList[i],SERIES_SERVER_FIRSTDATE);
   }
   
   int sIndex =0,eIndex=0;
   for(int i=0;i<9;i++)
   {
      if( swingTime >= maxTime[i])
      {
         sIndex = i;
         break;
      }
   }
   for(int i=0;i<9;i++)
   {
      if(m_tfList[i] == m_period)
      {
         eIndex = i > sIndex ? i - 1 : sIndex;
         break;
      }
   }
   int total = ::ArrayResize(tf,eIndex-sIndex+1);
   for(int i=0;i<total;i++)
   {
      tf[i] = m_tfList[sIndex+i];
   }
   return total;
}

void CProZigZag::DrawOnChart(const bool draw_on_chart)
{
   m_draw_on_chart = draw_on_chart;
   if(draw_on_chart)
      RefreshZigData(true);
   
}
//+------------------------------------------------------------------+
void CProZigZag::Draw(void)
{
   int total = m_SwingSetList.Total();
   if(total > 0)
   {
      string name =   TimeToString(m_SwingSetList[0].TimeBegin()) 
                     +TimeToString(m_SwingSetList[0].TimeEnd())
                     +DoubleToStr(m_SwingSetList[0].PriceEnd(),m_digits)
                     +m_instance;
      
      CChartObjectTrend *trend = m_drawList.Search(name);
      if(trend != NULL)
         return;
      m_drawList.Clear();
      for(int i=0;i<total;i++)
      {   
         name =   TimeToString(m_SwingSetList[i].TimeBegin()) 
                     +TimeToString(m_SwingSetList[i].TimeEnd())
                     +DoubleToStr(m_SwingSetList[i].PriceEnd(),m_digits)
                     +m_instance;
         trend = new CChartObjectTrend();
         trend.Create(0,name,0,  m_SwingSetList[i].TimeBegin(),            // first point time
                                 m_SwingSetList[i].PriceBegin(),           // first point price
                                 m_SwingSetList[i].TimeEnd(),              // second point time
                                 m_SwingSetList[i].PriceEnd()
                     );
         trend.RayRight(false);
         trend.Color(m_color);
         m_drawList.Add(trend);
      }
   }
}

