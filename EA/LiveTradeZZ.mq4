//+------------------------------------------------------------------+
//|                                                  LiveTradeZZ.mq4 |
//|                                      Copyright 2017, nicholishen |
//|                         https://www.forexfactory.com/nicholishen |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, nicholishen"
#property link      "https://www.forexfactory.com/nicholishen"
#property version   "1.00"
#property strict

#include "ProZigZag.mqh"
CProZigZag ziggy1,ziggy2,ziggy3;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   ulong micro = GetMicrosecondCount();
 
   ziggy1.Create(_Symbol,(ENUM_TIMEFRAMES)PERIOD_H1,5);
   ziggy1.ResolveToLowestTimeFrame(false);
   ziggy1.BufferResize(10);
   ziggy1.DrawOnChart(true);
   ziggy2.Create(_Symbol,(ENUM_TIMEFRAMES)PERIOD_M5,5);
   ziggy2.SetColor(clrDodgerBlue);
   ziggy2.ResolveToLowestTimeFrame(false);
   ziggy2.BufferResize(10);
   ziggy2.DrawOnChart(true);
//   
//   ziggy3.Create(_Symbol,(ENUM_TIMEFRAMES)PERIOD_M1,12);
//   ziggy3.ResolveToLowestTimeFrame(false);
//   ziggy3.BufferResize(10);
//   ziggy3.DrawOnChart(true);
   
   Comment("Initialization took... ",GetMicrosecondCount() - micro," micro-seconds.");
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
   //ulong micro = GetMicrosecondCount();
   ziggy1.Refresh();
   ziggy2.Refresh();
   //ziggy3.Refresh();
   //micro = GetMicrosecondCount() - micro;
   //Comment("Refreshing Indicators... ",micro," micro-seconds.");
   
   CSwingSet *set1 = ziggy1[0];
   CSwingSet *set2 = ziggy2[0];
   //CSwingSet *set3 = ziggy3[0];
   static datetime last_trade = 0;
   if(set1.SwingType()== SWING_LOW && Bid >= set1.PriceEnd()+(fabs(set1.PriceBegin()-set1.PriceEnd())*.764))
   {
      if(last_trade != set1.TimeBegin())
      {
         if(OrderSend(_Symbol,OP_SELL,0.01,Bid,0,Bid+20*_Point,Bid-50*_Point)>=0)
            last_trade = set1.TimeBegin();
      }
   }
   else
   if(set1.SwingType()== SWING_HIGH && Bid <= set1.PriceEnd()-(fabs(set1.PriceEnd()-set1.PriceBegin())*.764))
   {
      if(last_trade != set1.TimeBegin())
      {
         if(OrderSend(_Symbol,OP_BUY,0.01,Ask,0,Ask-20*_Point,Ask+50*_Point)>=0)
            last_trade = set1.TimeBegin();
       
      }
   }
}
//+------------------------------------------------------------------+
