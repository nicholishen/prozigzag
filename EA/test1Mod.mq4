//+------------------------------------------------------------------+
//|                                                        test1.mq4 |
//|                                      Copyright 2017, nicholishen |
//|                         https://www.forexfactory.com/nicholishen |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, nicholishen"
#property link      "https://www.forexfactory.com/nicholishen"
#property version   "1.00"
#property strict

#include <Indicators\ProZigZagMod.mqh>
CProZigZag ziggy1,ziggy2,ziggy3;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   ulong micro = GetMicrosecondCount();
 
   ziggy1.Create(_Symbol,(ENUM_TIMEFRAMES)PERIOD_M15,12);
   ziggy1.BufferResize(10);
   ziggy1.DrawOnChart(true);
   ziggy2.Create(_Symbol,(ENUM_TIMEFRAMES)PERIOD_M5,12);
   ziggy2.BufferResize(10);
   ziggy2.DrawOnChart(true);
   ziggy3.Create(_Symbol,(ENUM_TIMEFRAMES)PERIOD_M1,12);
   ziggy3.BufferResize(10);
   ziggy3.DrawOnChart(true);
   
   Comment("Initialization took... ",GetMicrosecondCount() - micro," micro-seconds.");
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   ulong micro = GetMicrosecondCount();
   ziggy1.Refresh();
   ziggy2.Refresh();
   ziggy3.Refresh();
   micro = GetMicrosecondCount() - micro;
   Comment("Refreshing Indicators... ",micro," micro-seconds.");
  }
//+------------------------------------------------------------------+
